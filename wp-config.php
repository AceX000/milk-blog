<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'milk_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')qw+o^$k)?;VlVT,;d0}7U>FZ<0pj3n.P47p}U0vLdR{X)&L7HOxyyHo>`Qggj}5');
define('SECURE_AUTH_KEY',  '2`SlL|ilLmrwk$+637&LAiqT9oIE#2|U@1F;oO>`1 u:s>W4!]}:zuO|,hs=iQA6');
define('LOGGED_IN_KEY',    'kBgNEq^)I`/pYc8?n5-:V8]P)I>koLuxYTJ4cGK3cj$D0>n~z#%$Ib^U5rjI;EC%');
define('NONCE_KEY',        'GrL.0kpDopyV3BYn}]%&]vW2O_BS5c4LYsf^EvUf$u4&1>j*1gGeQMIZjIqe_bo_');
define('AUTH_SALT',        '>XiCM9p([M+b%Fg%WPpn6WAA!&;IQL&bu~?$w^suv1EE[ OVFz9Jbj&A]OBgQioc');
define('SECURE_AUTH_SALT', 'pne+jt:Kw9ifk1r)-%iS=@=I}[%1z ]ZZhrmySvf(&w!7K*G{w6iqe+Ea+0:V@y/');
define('LOGGED_IN_SALT',   '(^jH#@oq6!p+-`4N7?+LJW-w0S!;ye0_|[1;m(@SpD93|^ljYWbD)8@B5o%X067<');
define('NONCE_SALT',       'vn<_psD_1ld]*!&y/?#`U2A=7%JZ_R37ZzUL~a]MSR?B:c:^^2yu0i6^Qy02l04W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
